var previousHaikuContainer = null;
var generateHaiku = document.getElementsByClassName('generate-haiku')[0];
var haikuContainer = document.getElementById('haikuContainer');
var haikuTemplate = document.getElementById('haikuTemplate');
generateHaiku.addEventListener('click', function (event) {
    var req = new XMLHttpRequest();
    req.onload = function () {
        var newHaikuTemplate = haikuTemplate.cloneNode(true);
        if(previousHaikuContainer != null) {
            var classes = previousHaikuContainer.className;
            classes = classes.replace('newest-haiku', '').trim();
            previousHaikuContainer.className = classes;
        }
        newHaikuTemplate.className = newHaikuTemplate.className + ' newest-haiku';
        var haikuText = newHaikuTemplate.getElementsByClassName('haikuText')[0];
        haikuText.innerHTML = this.responseText;
        newHaikuTemplate = haikuContainer.insertBefore(newHaikuTemplate, previousHaikuContainer);
        previousHaikuContainer = newHaikuTemplate;
        newHaikuTemplate.removeAttribute('hidden');
        newHaikuTemplate.removeAttribute('id');
    };
    req.open('get', '/haiku', true);
    req.send();
});