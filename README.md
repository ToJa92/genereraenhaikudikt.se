#GenereraEnHaikuDikt.se

GenereraEnHaikuDikt.se is a website which generates haiku at the press of a button.

##Requirements

These versions was used when developing the system.

- Python 3.4
- Flask 0.10.1

##Running

In the root folder, execute the following command:
```
python Haiku.py
```
