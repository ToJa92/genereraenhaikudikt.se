from flask import Flask, render_template
from HaikuGenerator.haikugenerator import HaikuGenerator

app = Flask(__name__)
haiku = HaikuGenerator()
haiku.load_db('HaikuGenerator/Haikudikter.txt')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/haiku')
def get_haiku():
    haiku_stuff = haiku.generate_haiku()
    return ' '.join(haiku_stuff[0]) + '<br>' + ' '.join(haiku_stuff[1]) + '<br>' + ' '.join(haiku_stuff[2])

if __name__ == '__main__':
    app.run()
