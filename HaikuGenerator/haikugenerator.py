from collections import defaultdict
from random import randint, choice
from re import findall
from pprint import pprint

def replace_all(s, l):
    for e in l:
        s = s.replace(e, '')
    return s

class HaikuGenerator():
    VOWELS = ['a', 'e', 'i', 'o', 'u', 'y', 'å', 'ä', 'ö']

    def __init__(self):
        self.initial_words = defaultdict(list)
        self.middle_words = defaultdict(list)
        self.last_words = defaultdict(list)
        self.bigram = defaultdict(list)

    def load_db(self, file_name):
        f = open(file_name, 'r')
        for line in f.readlines():
            line = replace_all(line, ['.',';',',','"', '”', '“', ':'])
            line = line.strip().lower()
            if not line:
                continue
            tokens = line.split()
            for i in range(0, len(tokens)):
                token_syllables = self.count_syllables(tokens[i])
                if token_syllables == 0:
                    continue
                if i == 0:
                    self.initial_words[token_syllables].append(tokens[i])
                elif i == len(tokens) - 1:
                    self.last_words[token_syllables].append(tokens[i])
                else:
                    self.middle_words[token_syllables].append(tokens[i])
                if i != 0:
                    self.bigram[tokens[i-1]].append(tokens[i])
        for i in self.initial_words:
            self.initial_words[i] = list(set(self.initial_words[i]))
        for i in self.last_words:
            self.last_words[i] = list(set(self.last_words[i]))
        for i in self.middle_words:
            self.middle_words[i] = list(set(self.middle_words[i]))

    def count_syllables(self, word):
        sum = 0
        for v in self.VOWELS:
            sum += word.count(v)
        return sum

    def generate_haiku(self, first_start_word = None, middle_start_word = None, last_start_word = None):
        haiku = []
        if first_start_word:
            haiku.append(self.__generate_sentence(5 - self.count_syllables(first_start_word), [first_start_word]))
        else:
            haiku.append(self.__generate_sentence(5, []))
        if middle_start_word:
            haiku.append(self.__generate_sentence(7 - self.count_syllables(middle_start_word), [middle_start_word]))
        else:
            haiku.append(self.__generate_sentence(7, []))
        if last_start_word:
            haiku.append(self.__generate_sentence(5 - self.count_syllables(last_start_word), [last_start_word]))
        else:
            haiku.append(self.__generate_sentence(5, []))
        haiku[0][0] = haiku[0][0].title()
        return haiku

    def generate_haiku2(self, first_start_word = None, middle_start_word = None, last_start_word = None):
        haiku = []
        if first_start_word:
            haiku.append(self.__generate_sentence2(5 - self.count_syllables(first_start_word), [first_start_word]))
        else:
            haiku.append(self.__generate_sentence2(5, []))
        if middle_start_word:
            haiku.append(self.__generate_sentence2(7 - self.count_syllables(middle_start_word), [middle_start_word]))
        else:
            haiku.append(self.__generate_sentence2(7, []))
        if last_start_word:
            haiku.append(self.__generate_sentence2(5 - self.count_syllables(last_start_word), [last_start_word]))
        else:
            haiku.append(self.__generate_sentence2(5, []))
        haiku[0][0] = haiku[0][0].title()
        return haiku

    def __generate_sentence(self, num_syllables, haiku_sentence):
        syllable_list = []
        while num_syllables > 0:
            syllable_count = randint(1, min(5, num_syllables))
            if not haiku_sentence:
                syllable_list = self.initial_words
            elif num_syllables - syllable_count == 0:
                syllable_list = self.last_words
            else:
                syllable_list = self.middle_words
            if syllable_count not in syllable_list:
                continue
            if haiku_sentence:
                if haiku_sentence[-1] in self.bigram:
                    previous_word_bigrams = self.bigram[haiku_sentence[-1]]
                    common_words = list(set(previous_word_bigrams) & set(syllable_list[syllable_count]))
                    if common_words:
                        haiku_sentence.append(choice(common_words))
                        num_syllables -= syllable_count
                        continue
                    else:
                        prev_word = haiku_sentence.pop()
                        num_syllables += self.count_syllables(prev_word)
                        continue
                else:
                    prev_word = haiku_sentence.pop()
                    num_syllables += self.count_syllables(prev_word)
                    continue
            haiku_sentence.append(choice(syllable_list[syllable_count]))
            num_syllables -= syllable_count
        return haiku_sentence

    def __generate_sentence2(self, num_syllables, haiku_sentence):
        syllable_list = []
        while num_syllables > 0:
            syllable_count = randint(1, num_syllables)
            if not haiku_sentence:
                syllable_list = self.initial_words
            elif num_syllables - syllable_count == 0:
                syllable_list = self.last_words
            else:
                syllable_list = self.middle_words
            if haiku_sentence:
                if haiku_sentence[-1] in self.bigram:
                    previous_word_bigrams = self.bigram[haiku_sentence[-1]]
                    previous_word_bigrams = [i for i in previous_word_bigrams if self.count_syllables(i) <= num_syllables]
                    if previous_word_bigrams:
                        haiku_sentence.append(choice(previous_word_bigrams))
                        num_syllables -= self.count_syllables(haiku_sentence[-1])
                        continue
            if syllable_count not in syllable_list:
                continue
            haiku_sentence.append(choice(syllable_list[syllable_count]))
            num_syllables -= syllable_count
        return haiku_sentence

if __name__ == '__main__':
    haiku = HaikuGenerator()
    haiku.load_db('Haikudikter.txt')
    #first_start_word = choice(sum(haiku.initial_words.values(), []))
    #middle_start_word = choice(sum(haiku.initial_words.values(), []))
    #last_start_word = choice(sum(haiku.initial_words.values(), []))
    for i in range(0, 500):
        #haiku.generate_haiku()
        for j in haiku.generate_haiku():
            print(' '.join(j))
        print()
    #print('-------------------------------------------')
    #for i in range(0, 30):
    #    for j in haiku.generate_haiku2():
    #        print(' '.join(j))
    #    print()